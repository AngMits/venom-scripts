#!/bin/sh
## Copyright (c) 2024 by mobinmob  <mobinmob@disroot.org
##This program is free software: you can redistribute it and/or modify it under
## the terms of the GNU General Public License as published by the Free 
## Software Foundation, either version 2.0 of the License, or (at your option)
## any later version.
## This program is distributed in the hope that it will be useful, but WITHOUT
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
## FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
## You should have received a copy of the GNU General Public License along with
## this program. If not, see <https://www.gnu.org/licenses/>.

# Default dirs and general data for the script operation
script_dir="$(dirname "$(realpath "$0")")"
# Default values for variables that control prog operation
[ "$op_buildintmpfs" != "1" ] && op_buildintmpfs=""

error() {
	printf "(!!) Error: %s\n" "$*" && exit 1
}

warn() {
	printf "(~~) Warning: %s\n" "$*"
}

info() {
	printf "(~>) Info: %s\n" "$*" 
}

usage() {
	cat <<-EOH
	Usage: $(basename "$0") [options]

	*p*ac*k*age *build* in a venom container
	Build one or more packages for Venom Linux in a container.
	
	OPTIONS
	 -p <package1,package2>	Build a list of (comma-seperated) packages
	 -h	Display this usage information.
	EOH
}

scratch_build_packages() {
	package_list="$(echo "$packages" | tr ',' ' ')"
	[ -z "$package_list" ] && warn Not building packages, just upgrading rootfs.
	for i in $package_list; do 
		scratch install "$i" -y 
		pkgbase -y
	done
}

main() {
	info Full comand given is [pkbuild-container.sh "$showargs"].
	[ "$(id -u)" -ne 0 ] && error You need to be root to run this program!
	cd "$script_dir" || exit
	command -v scratch > /dev/null || error No 'scratch' available, aborting!
	scratch sync -y
	# shellcheck disable=2181
	[ "$?" -ne 0 ] && warn Repos did not sync, see above..
	scratch upgrade scratchpkg -y
	# shellcheck disable=2181
	[ "$?" -ne 0 ] && warn scratchpkg did not self-upgrade, see above...
	scratch sysup -y
	# shellcheck disable=2181
	[ "$?" -ne 0 ] && warn scratch did not perform a system upgrade, see above...
	scratch_build_packages
	scratch sysup -y
	# shellcheck disable=2181
	[ "$?" -ne 0 ] && warn scratch did not perform a system upgrade, see above...
	revdep -r -y
	pkgdir=/var/cache/scratchpkg/packages
	sha512sum $pkgdir/* > $pkgdir/packages.sha512sum 
}

while getopts "p:fh" OPTION; do
	case "${OPTION}" in
		p) packages="$OPTARG";;
		h) usage ; exit 0;;
		*) usage ; exit 0;;
	esac
done
# Capture all the arguments given
showargs="$(printf "%s" "$*")"
shift $((OPTIND - 1))
[ "$OPTIND" -eq 1 ] && usage && exit
main "$@"